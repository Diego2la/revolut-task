### Overview

Test task from Revolut for money transfers between accounts.
Abstract system has predefined:

1. currencies: RUB, USD.
2. 5.000 persons with nodeid=[0..4], id=[0..999].

Objects model created with considering further app run on multiple nodes.
That's why account and person ids consists of (node_id, id). Now only one node supported.
While executing money transfer module don't interact with any external system like bank or clearing operator (except another node in future).
Let's imagine for abrasions of task that we have deal with bonuses, balls but not real currencies.

### Run app

App use 4567 port for API exposing and 6383 port for Redis data storage.

1. git clone git@bitbucket.org:Diego2la/revolut-task.git
2. cd revolut-task
3. ./gradlew build
4. ./gradlew run

### REST API
Module has 4 HTTP methods listed below.
Each method returns json response with common structure:
```json
{
  "header": {
    "code": 2 # Error code
    "message": "Field missing", # Error description
    "params": {
      "field": "amount" # Extra description params
    }
  },
  "data": <any object>
}
```
If header.code is not zero than error has occurred.
All possible errors listed in section "Error Codes".

###### POST /account/add
Creates account with empty balance for person.

Request json format:
```json
{
  "person-id": {"node-id": 1, "id": 60},
  "currency-code": "USD",
  "account-name": "My main account"
}
```
If success, json response will be:
```json
{
  "header": {
    "code": 0,
    "message": "Account successfully created"  
  },
  "data": {
    "nodeId": 1,
    "id": 1
  }
}

```

###### GET /account/get-balance
Receive balance of person's account.

Request url params:
* account-id.node-id=1
* account-id.id=1

If success, json response will be:
```json
{
  "header": {
    "code": 0,
    "message": "Retrieving amount complete"  
  },
  "data": 0
}
```

###### POST /transfer/emission
Emit brand new money to specified account.

Request json format:
```json
{
  "credit-account-id": {"node-id": 1, "id": 1},
  "amount": "30"
}
```

###### POST /transfer/add
Perform money transfer between specified accounts.

Request json format:
```json
{
  "debit-account-id": {"node-id": 1, "id": 1},
  "credit-account-id": {"node-id": 1, "id": 2},
  "amount": "30"
}
```

### Error Codes

| Code | Message        | Params  |
|------|----------------|---------|
| 0    | 'message depends on method' |
| 1    | Internal error      |
| 2    | Field missing               | field
| 3    | Invalid json format               |
| 4    | Invalid field type               | field
| 5    | Invalid field format               | field
| 6    | Person node-id not suitable               | node-id
| 7    | Person not found               |
| 8    | Currency not found               |
| 9    | Account not found               |
| 10    | Account node-id not suitable               | node-id
| 11    | Cross node id transfers not implemented yet               |
| 12   | Not positive amount               |
| 13    | Different currencies accounts               |
| 14    | Invalid amount precision               |
| 15    | Not enough money               |
| 16    | Debit and credit accounts are equals               |



