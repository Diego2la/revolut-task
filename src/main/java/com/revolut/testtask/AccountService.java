package com.revolut.testtask;

import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.revolut.testtask.model.Account;
import com.revolut.testtask.model.ApiResult;
import com.revolut.testtask.model.CrossNodeId;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import spark.QueryParamsMap;

import java.math.BigDecimal;

import static com.revolut.testtask.ApiError.*;
import static org.eclipse.jetty.http.HttpStatus.Code.CREATED;

public class AccountService extends BaseService implements IAccountService {

    public static String ATOMIC_LONG_ACCOUNT_SEQ = "account-seq";
    public static String MAP_ACCOUNTS = "accounts";

    @Inject
    public AccountService(Config config, Dictionary dictionary,
                          EmbeddedRedisHolder redisHolder) {
        super(config, dictionary, redisHolder);
    }

    @Override
    public ApiResult create(String body) throws HandlerException {
        JsonObject root = parser.parseJson(body);
        CrossNodeId personId = parser.parseCrossNodeId(root ,"person-id");
        String currencyCode = parser.parseString(root ,"currency-code");
        String accountName = parser.parseOptionalString(root ,"account-name");

        if (personId.getNodeId() != config.getNodeId()) {
            throw new HandlerException(personNodeIdNotSuitable(personId.getNodeId()));
        }
        if (!dictionary.personExists(personId)) {
            throw new HandlerException(personNotFound());
        }
        if (!dictionary.currencyCodeExists(currencyCode)) {
            throw new HandlerException(currencyNotFound());
        }
        RedissonClient redisson = redis.getRedisson();
        Account account = new Account();
        RAtomicLong nextAccountId = redisson.getAtomicLong(ATOMIC_LONG_ACCOUNT_SEQ);
        CrossNodeId accountId = new CrossNodeId(config.getNodeId(), nextAccountId.incrementAndGet());
        account.setId(accountId);
        account.setPersonId(personId);
        account.setCurrencyCode(currencyCode);
        account.setName(accountName);
        account.setAmount(BigDecimal.ZERO);
        account.setTsCreated(System.currentTimeMillis());
        RMap<CrossNodeId, Account> accounts = redisson.getMap(MAP_ACCOUNTS);
        accounts.put(accountId, account);

        return new ApiResult(ok(CREATED, "Account successfully created"), accountId);
    }

    @Override
    public void updateAccountBalance(CrossNodeId accountId, BigDecimal additive) throws HandlerException {
        Account account = getAccount(accountId);
        BigDecimal amount = account.getAmount();
        account.setAmount(amount.add(additive));
        RMap<CrossNodeId, Account> accountMap = redis.getRedisson().getMap(MAP_ACCOUNTS);
        accountMap.put(accountId, account);
    }

    @Override
    public ApiResult getBalance(QueryParamsMap map) throws HandlerException {
        CrossNodeId crossNodeId = parseCrossNodeIdFromParams(map, "account-id");
        validateAccountOrigin(crossNodeId.getNodeId());
        Account account = getAccount(crossNodeId);
        return new ApiResult(ok("Retrieving amount complete"), account.getAmount());
    }

    @Override
    public void validateAccountOrigin(Long nodeId) throws HandlerException {
        if (nodeId != config.getNodeId()) {
            throw new HandlerException(accountNodeIdNotSuitable(nodeId));
        }
    }

    @Override
    public Account getAccount(CrossNodeId id) throws HandlerException {
        RMap<CrossNodeId, Account> accountMap = redis.getRedisson().getMap(MAP_ACCOUNTS);
        Account account = accountMap.get(id);
        if (account == null) {
            throw new HandlerException(accountNotFound());
        }
        return account;
    }

    private CrossNodeId parseCrossNodeIdFromParams(QueryParamsMap map, String key) throws HandlerException {
        CrossNodeId crossNodeId = new CrossNodeId();
        crossNodeId.setId(parseLong(map, key + ".id"));
        crossNodeId.setNodeId(parseLong(map, key + ".node-id"));
        return crossNodeId;
    }

    private Long parseLong(QueryParamsMap map, String key) throws HandlerException {
        String res = map.value(key);
        if (res == null) {
            throw new HandlerException(fieldMissing(key));
        }
        try {
            return Long.parseLong(res);
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(key));
        }
    }
}
