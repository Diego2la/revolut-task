package com.revolut.testtask;

import com.revolut.testtask.model.ApiHeader;
import org.eclipse.jetty.http.HttpStatus.Code;

import static org.eclipse.jetty.http.HttpStatus.Code.BAD_REQUEST;
import static org.eclipse.jetty.http.HttpStatus.Code.INTERNAL_SERVER_ERROR;
import static org.eclipse.jetty.http.HttpStatus.Code.NOT_IMPLEMENTED;

public class ApiError {

    public static ApiHeader ok(String okText) {
        return ok(Code.OK, okText);
    }

    public static ApiHeader ok(Code code, String text) {
        return new ApiHeader(0, code, text);
    }

    public static ApiHeader internalError() {
        return new ApiHeader(1, INTERNAL_SERVER_ERROR, "Internal error");
    }

    public static ApiHeader fieldMissing(String field) {
        return new ApiHeader(2, BAD_REQUEST, "Field missing")
                .withParam("field", field);
    }

    public static ApiHeader invalidJsonFormat() {
        return new ApiHeader(3, BAD_REQUEST, "Invalid json format");
    }

    public static ApiHeader invalidFieldType(String field) {
        return new ApiHeader(4, BAD_REQUEST, "Invalid field type")
                .withParam("field", field);
    }

    public static ApiHeader invalidFieldFormat(String field) {
        return new ApiHeader(5, BAD_REQUEST, "Invalid field format")
                .withParam("field", field);
    }

    public static ApiHeader personNodeIdNotSuitable(Long nodeId) {
        return new ApiHeader(6, BAD_REQUEST, "Person node-id not suitable")
                .withParam("node-id", nodeId.toString());
    }

    public static ApiHeader personNotFound() {
        return new ApiHeader(7, BAD_REQUEST, "Person not found");
    }

    public static ApiHeader currencyNotFound() {
        return new ApiHeader(8, BAD_REQUEST, "Currency not found");
    }

    public static ApiHeader accountNotFound() {
        return new ApiHeader(9, BAD_REQUEST, "Account not found");
    }

    public static ApiHeader accountNodeIdNotSuitable(Long nodeId) {
        return new ApiHeader(10, BAD_REQUEST, "Account node-id not suitable")
                .withParam("node-id", nodeId.toString());
    }

    public static ApiHeader crossNodeIdTransfersNotImplementedYet() {
        return new ApiHeader(11, NOT_IMPLEMENTED,
                "Cross node id transfers not implemented yet");
    }

    public static ApiHeader notPositiveAmount() {
        return new ApiHeader(12, BAD_REQUEST, "Not positive amount");
    }

    public static ApiHeader differentCurrenciesAccounts() {
        return new ApiHeader(13, BAD_REQUEST, "Different currencies accounts");
    }

    public static ApiHeader invalidAmountPrecision() {
        return new ApiHeader(14, BAD_REQUEST, "Invalid amount precision");
    }

    public static ApiHeader notEnoughMoney() {
        return new ApiHeader(15, BAD_REQUEST, "Not enough money");
    }

    public static ApiHeader debitAndCreditAccountsAreEquals() {
        return new ApiHeader(16, BAD_REQUEST,
                "Debit and credit accounts are equals");
    }
}
