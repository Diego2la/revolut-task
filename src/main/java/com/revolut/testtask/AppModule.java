package com.revolut.testtask;

import com.google.inject.Binder;
import com.google.inject.Module;

public class AppModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(IAccountService.class).to(AccountService.class);
        binder.bind(ITransferService.class).to(TransferService.class);
        binder.bind(Config.class).asEagerSingleton();
        binder.bind(Dictionary.class).asEagerSingleton();
        binder.bind(EmbeddedRedisHolder.class).asEagerSingleton();
    }
}
