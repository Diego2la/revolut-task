package com.revolut.testtask;

import static com.revolut.testtask.ApiError.accountNodeIdNotSuitable;

public class BaseService {

    protected JsonBodyParser parser = new JsonBodyParser();

    protected Config config;
    protected Dictionary dictionary;
    protected EmbeddedRedisHolder redis;

    public BaseService(Config config, Dictionary dictionary,
                       EmbeddedRedisHolder redisHolder) {
        this.config = config;
        this.dictionary = dictionary;
        this.redis = redisHolder;
    }

    public EmbeddedRedisHolder getRedis() {
        return redis;
    }
}

