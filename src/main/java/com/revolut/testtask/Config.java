package com.revolut.testtask;

import com.beust.jcommander.Parameter;

public class Config {

    @Parameter(required = true, names = { "-node-id" }, description = "Node Id of app instance")
    private Long nodeId;

    public Config() {
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getNodeId() {
        return nodeId;
    }
}
