package com.revolut.testtask;

import com.revolut.testtask.model.CrossNodeId;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Dictionary {

    public static DictionaryCurrency CURRENCY_USD = new DictionaryCurrency("USD", new BigDecimal("0.01"));
    public static DictionaryCurrency CURRENCY_RUB = new DictionaryCurrency("RUB", new BigDecimal("0.01"));

    private Set<CrossNodeId> personIds = new HashSet<>();
    private Map<String, DictionaryCurrency> currencies = new HashMap<>();

    public Dictionary() {
        for (long nodeId = 0; nodeId < 5; ++nodeId) {
            for (long id = 0; id < 1000; ++id) {
                personIds.add(new CrossNodeId(nodeId, id));
            }
        }
        currencies.put(CURRENCY_USD.getCode(), CURRENCY_USD);
        currencies.put(CURRENCY_RUB.getCode(), CURRENCY_RUB);
    }

    public boolean personExists(CrossNodeId cnid) {
        return personIds.contains(cnid);
    }

    public boolean currencyCodeExists(String currencyCode) {
        return currencies.containsKey(currencyCode);
    }

    public DictionaryCurrency getCurrency(String currencyCode) {
        return currencies.get(currencyCode);
    }
}
