package com.revolut.testtask;

import java.math.BigDecimal;

public class DictionaryCurrency {

    private String code;
    private BigDecimal minStep;

    public DictionaryCurrency(String cide, BigDecimal minStep) {
        this.code = cide;
        this.minStep = minStep;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getMinStep() {
        return minStep;
    }
}
