package com.revolut.testtask;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.embedded.RedisServer;

import java.io.IOException;

public class EmbeddedRedisHolder {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmbeddedRedisHolder.class);

    private volatile RedisServer redisServer;
    private RedissonClient redisson;

    public void start() throws IOException {
        LOGGER.info("Start embedded Redis");
        int port = 6383;
        redisServer = new RedisServer(port);
        redisServer.start();

        org.redisson.config.Config config = new org.redisson.config.Config();
        config.useSingleServer().setAddress("redis://127.0.0.1:" + port);
        redisson = Redisson.create(config);
    }

    public void stop() {
        LOGGER.info("Stop embedded Redis");
        redisson.shutdown();
        redisServer.stop();
    }

    public RedissonClient getRedisson() {
        return redisson;
    }
}
