package com.revolut.testtask;

import com.revolut.testtask.model.ApiHeader;

public class HandlerException extends Exception {

    private ApiHeader header;

    public HandlerException(ApiHeader header) {
        this.header = header;
    }

    public ApiHeader getHeader() {
        return header;
    }
}
