package com.revolut.testtask;

import com.revolut.testtask.model.Account;
import com.revolut.testtask.model.ApiResult;
import com.revolut.testtask.model.CrossNodeId;
import spark.QueryParamsMap;

import java.math.BigDecimal;

public interface IAccountService {
    ApiResult create(String body) throws HandlerException;

    void updateAccountBalance(CrossNodeId accountId, BigDecimal additive) throws HandlerException;

    ApiResult getBalance(QueryParamsMap map) throws HandlerException;

    void validateAccountOrigin(Long nodeId) throws HandlerException;

    Account getAccount(CrossNodeId id) throws HandlerException;
}
