package com.revolut.testtask;

import com.revolut.testtask.model.ApiResult;

public interface ITransferService {
    ApiResult add(String body) throws HandlerException;
    ApiResult emission(String body) throws HandlerException;
}
