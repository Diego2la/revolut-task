package com.revolut.testtask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.revolut.testtask.model.CrossNodeId;

import java.math.BigDecimal;

import static com.revolut.testtask.ApiError.*;

public class JsonBodyParser {

    public JsonObject parseJson(String body) throws HandlerException {
        JsonObject rootObject;
        try {
            rootObject = new JsonParser().parse(body).getAsJsonObject();
        } catch (IllegalStateException | JsonParseException e) {
            throw new HandlerException(invalidJsonFormat());
        }
        return rootObject;
    }

    public CrossNodeId parseCrossNodeId(JsonObject rootObject, String field) throws HandlerException {
        JsonElement jsonElement = elem(rootObject, field);
        JsonObject object;
        try {
            object = jsonElement.getAsJsonObject();
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(field));
        }
        try {
            JsonElement elemNodeId = object.get("node-id");
            JsonElement elemId = object.get("id");
            if (elemNodeId == null || elemId == null) {
                throw new HandlerException(invalidFieldFormat(field));
            }
            long nodeId = elemNodeId.getAsLong();
            long id = elemId.getAsLong();
            return new CrossNodeId(nodeId, id);
        } catch (Exception e) {
            throw new HandlerException(invalidFieldFormat(field));
        }
    }

    private JsonElement elem(JsonObject root, String field) throws HandlerException {
        JsonElement jsonElement = root.get(field);
        if (jsonElement == null) {
            throw new HandlerException(fieldMissing(field));
        }
        return jsonElement;
    }

    public Long parseLong(JsonObject root, String field) throws HandlerException {
        JsonElement elem = elem(root, field);
        Long res;
        try {
            res = elem.getAsLong();
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(field));
        }
        return res;
    }

    public BigDecimal parseBigDecimal(JsonObject root, String field) throws HandlerException {
        JsonElement elem = elem(root, field);
        BigDecimal res;
        try {
            res = elem.getAsBigDecimal();
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(field));
        }
        return res;
    }

    public String parseString(JsonObject root, String field) throws HandlerException {
        JsonElement elem = elem(root, field);
        String res;
        try {
            res = elem.getAsString();
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(field));
        }
        return res;
    }

    public String parseOptionalString(JsonObject root, String field) throws HandlerException {
        JsonElement jsonElement = root.get(field);
        if (jsonElement == null) {
            return null;
        }
        String res;
        try {
            res = jsonElement.getAsString();
        } catch (Exception e) {
            throw new HandlerException(invalidFieldType(field));
        }
        return res;
    }
}
