package com.revolut.testtask;

import com.beust.jcommander.JCommander;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.revolut.testtask.model.ApiHeader;
import com.revolut.testtask.model.ApiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Response;

import java.io.IOException;

import static com.revolut.testtask.ApiError.internalError;
import static com.revolut.testtask.util.JsonUtil.json;
import static spark.Spark.*;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private IAccountService accountService;
    private ITransferService transferService;
    private Config config;
    private EmbeddedRedisHolder embeddedRedisHolder;

    @Inject
    public Main(IAccountService accountService, Config config,
                EmbeddedRedisHolder embeddedRedisHolder, ITransferService transferService) {
        this.accountService = accountService;
        this.transferService = transferService;
        this.config = config;
        this.embeddedRedisHolder = embeddedRedisHolder;
    }

    public void run() throws IOException {
        embeddedRedisHolder.start();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> embeddedRedisHolder.stop()));
        post("/account/add", (req, res) -> {
            ApiResult result = accountService.create(req.body());
            return wrapResult(res, result);
            }, json());
        get("/account/get-balance", (req, res) -> {
            ApiResult result = accountService.getBalance(req.queryMap());
            return wrapResult(res, result);
        }, json());
        post("/transfer/add", (req, res) -> {
            ApiResult result = transferService.add(req.body());
            return wrapResult(res, result);
        }, json());
        post("/transfer/emission", (req, res) -> {
            ApiResult result = transferService.emission(req.body());
            return wrapResult(res, result);
        }, json());
        after((req, res) -> res.type("application/json"));

        exception(HandlerException.class, (e, req, res) -> {
            processException(res, e.getHeader());
        });
        exception(Exception.class, (e, req, res) -> {
            LOGGER.warn("Got unhandled exception", e);
            processException(res, internalError());
        });
    }

    private void processException(Response res, ApiHeader header) {
        ApiResult result = wrapResult(res, new ApiResult(header));
        res.type("application/json");
        try {
            res.body(json().render(result));
        } catch (Exception e1) {
            LOGGER.warn("Failed to render json while handling Exception");
        }
    }

    private ApiResult wrapResult(Response res, ApiResult result) {
        ApiHeader header = result.getHeader();
        res.status(header.getHttpCode().getCode());
        header.setHttpCode(null);
        return result;
    }

    public Config getConfig() {
        return config;
    }

    public static void main(String args[]) throws IOException {
        Injector injector = Guice.createInjector(new AppModule());
        main(injector, args);
    }

    public static Main main(Injector injector, String args[]) throws IOException {
        Main main = injector.getInstance(Main.class);
        Config config = main.getConfig();
        JCommander.newBuilder()
                .addObject(config)
                .build()
                .parse(args);
        main.run();
        return main;
    }

    public IAccountService getAccountService() {
        return accountService;
    }
}
