package com.revolut.testtask;

import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.revolut.testtask.model.*;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import java.math.BigDecimal;

import static com.revolut.testtask.ApiError.*;
import static org.eclipse.jetty.http.HttpStatus.Code.CREATED;

public class TransferService extends BaseService implements ITransferService {

    public static String ATOMIC_LONG_TRANSFER_SEQ = "transfer-seq";
    public static String MAP_TRANSFERS = "transfers";

    private IAccountService accountService;

    @Inject
    public TransferService(Config config, Dictionary dictionary,
                          EmbeddedRedisHolder redisHolder, IAccountService accountService) {
        super(config, dictionary, redisHolder);
        this.accountService = accountService;
    }

    @Override
    public ApiResult emission(String body) throws HandlerException {
        JsonObject root = parser.parseJson(body);
        CrossNodeId creditAcc = parser.parseCrossNodeId(root ,"credit-account-id");
        BigDecimal amount = parser.parseBigDecimal(root, "amount");
        validatePositiveAmount(amount);
        accountService.validateAccountOrigin(creditAcc.getNodeId());
        Account creditAccount = accountService.getAccount(creditAcc);
        String currencyCode = creditAccount.getCurrencyCode();
        validateAmountMinStep(amount, currencyCode);

        Transfer transfer = new Transfer();
        transfer.setType(TransferType.EMISSION);
        transfer.setAccountId(creditAcc);
        transfer.setCpAccountId(null);
        transfer.setAmount(amount);
        transfer.setTsCreated(System.currentTimeMillis());
        transfer.setCurrencyCode(currencyCode);
        saveNewTransfer(transfer);
        accountService.updateAccountBalance(creditAcc, amount);
        return new ApiResult(ok(CREATED, "Emission performed successfully"));
    }

    private void saveNewTransfer(Transfer transfer) {
        RedissonClient redisson = redis.getRedisson();
        RAtomicLong nextTransactionId = redisson.getAtomicLong(ATOMIC_LONG_TRANSFER_SEQ);
        CrossNodeId transferId = new CrossNodeId(config.getNodeId(), nextTransactionId.incrementAndGet());
        transfer.setId(transferId);
        RMap<CrossNodeId, Transfer> transfers = redisson.getMap(MAP_TRANSFERS);
        transfers.put(transferId, transfer);
    }

    @Override
    public ApiResult add(String body) throws HandlerException {
        JsonObject root = parser.parseJson(body);
        CrossNodeId debitAcc = parser.parseCrossNodeId(root ,"debit-account-id");
        CrossNodeId creditAcc = parser.parseCrossNodeId(root ,"credit-account-id");
        BigDecimal amount = parser.parseBigDecimal(root, "amount");
        validatePositiveAmount(amount);
        accountService.validateAccountOrigin(debitAcc.getNodeId());
        if (creditAcc.getNodeId() != config.getNodeId()) {
            throw new HandlerException(crossNodeIdTransfersNotImplementedYet());
        }
        Account debitAccount = accountService.getAccount(debitAcc);
        Account creditAccount = accountService.getAccount(creditAcc);
        if (debitAccount.getId().equals(creditAccount.getId())) {
            throw new HandlerException(debitAndCreditAccountsAreEquals());
        }
        String currencyCode = debitAccount.getCurrencyCode();
        if (!currencyCode.equals(creditAccount.getCurrencyCode())) {
            throw new HandlerException(differentCurrenciesAccounts());
        }
        validateAmountMinStep(amount, currencyCode);
        if (debitAccount.getAmount().compareTo(amount) < 0) {
            throw new HandlerException(notEnoughMoney());
        }

        long tsCreated = System.currentTimeMillis();
        Transfer transferDebit = new Transfer();
        transferDebit.setType(TransferType.WITHDRAW);
        transferDebit.setAccountId(debitAcc);
        transferDebit.setCpAccountId(creditAcc);
        transferDebit.setAmount(amount);
        transferDebit.setTsCreated(tsCreated);
        transferDebit.setCurrencyCode(currencyCode);
        saveNewTransfer(transferDebit);

        Transfer transferCredit = new Transfer();
        transferCredit.setType(TransferType.INPUT);
        transferCredit.setAccountId(creditAcc);
        transferCredit.setCpAccountId(debitAcc);
        transferCredit.setAmount(amount);
        transferCredit.setTsCreated(tsCreated);
        transferCredit.setCurrencyCode(currencyCode);
        saveNewTransfer(transferCredit);

        accountService.updateAccountBalance(debitAcc, amount.multiply(new BigDecimal(-1)));
        accountService.updateAccountBalance(creditAcc, amount);
        return new ApiResult(ok(CREATED, "Transfer performed successfully"));
    }

    private void validatePositiveAmount(BigDecimal amount) throws HandlerException {
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new HandlerException(notPositiveAmount());
        }
    }

    private void validateAmountMinStep(BigDecimal amount, String currencyCode) throws HandlerException {
        DictionaryCurrency currency = dictionary.getCurrency(currencyCode);
        BigDecimal minStep = currency.getMinStep();
        if (BigDecimal.ZERO.compareTo(amount.remainder(minStep)) != 0) {
            throw new HandlerException(invalidAmountPrecision());
        }
    }
}
