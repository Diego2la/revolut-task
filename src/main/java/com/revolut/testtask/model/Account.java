package com.revolut.testtask.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Account {

    private CrossNodeId id;

    private CrossNodeId personId;

    private String currencyCode;

    private String name;

    private BigDecimal amount;

    private long tsCreated;

    public CrossNodeId getId() {
        return id;
    }

    public void setId(CrossNodeId id) {
        this.id = id;
    }

    public CrossNodeId getPersonId() {
        return personId;
    }

    public void setPersonId(CrossNodeId personId) {
        this.personId = personId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getTsCreated() {
        return tsCreated;
    }

    public void setTsCreated(long tsCreated) {
        this.tsCreated = tsCreated;
    }
}
