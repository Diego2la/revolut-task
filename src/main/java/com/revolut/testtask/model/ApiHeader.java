package com.revolut.testtask.model;

import org.eclipse.jetty.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class ApiHeader {

    private Integer code;

    private HttpStatus.Code httpCode;

    private String message;

    private Map<String, String> params;

    public ApiHeader(Integer code, HttpStatus.Code httpCode, String message) {
        this.code = code;
        this.httpCode = httpCode;
        this.message = message;
    }

    public ApiHeader withParam(String key, String value) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put(key, value);
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public HttpStatus.Code getHttpCode() {
        return httpCode;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setHttpCode(HttpStatus.Code httpCode) {
        this.httpCode = httpCode;
    }
}
