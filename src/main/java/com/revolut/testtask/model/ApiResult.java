package com.revolut.testtask.model;

public class ApiResult<D> {

    private ApiHeader header;

    private D data;

    public ApiResult(ApiHeader header) {
        this.header = header;
    }

    public ApiResult(ApiHeader header, D data) {
        this.header = header;
        this.data = data;
    }

    public ApiHeader getHeader() {
        return header;
    }

    public D getData() {
        return data;
    }

}
