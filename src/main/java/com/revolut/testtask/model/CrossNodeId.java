package com.revolut.testtask.model;

import java.util.Objects;

public class CrossNodeId {

    private Long nodeId;

    private Long id;

    public CrossNodeId() {
    }

    public CrossNodeId(long nodeId, long id) {
        this.nodeId = nodeId;
        this.id = id;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CrossNodeId that = (CrossNodeId) o;
        return Objects.equals(nodeId, that.nodeId) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeId, id);
    }
}
