package com.revolut.testtask.model;

import java.math.BigDecimal;

public class Transfer {

    private CrossNodeId id;

    private TransferType type;

    private CrossNodeId accountId;

    private CrossNodeId cpAccountId;

    private BigDecimal amount;

    private long tsCreated;

    private String currencyCode;

    public CrossNodeId getId() {
        return id;
    }

    public void setId(CrossNodeId id) {
        this.id = id;
    }

    public TransferType getType() {
        return type;
    }

    public void setType(TransferType type) {
        this.type = type;
    }

    public CrossNodeId getAccountId() {
        return accountId;
    }

    public void setAccountId(CrossNodeId accountId) {
        this.accountId = accountId;
    }

    public CrossNodeId getCpAccountId() {
        return cpAccountId;
    }

    public void setCpAccountId(CrossNodeId cpAccountId) {
        this.cpAccountId = cpAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getTsCreated() {
        return tsCreated;
    }

    public void setTsCreated(long tsCreated) {
        this.tsCreated = tsCreated;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
