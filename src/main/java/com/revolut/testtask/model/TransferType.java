package com.revolut.testtask.model;

public enum TransferType {

    EMISSION,
    WITHDRAW,
    INPUT;
}
