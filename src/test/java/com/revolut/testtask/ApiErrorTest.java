package com.revolut.testtask;

import com.revolut.testtask.model.ApiHeader;
import com.revolut.testtask.util.JsonUtil;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.eclipse.jetty.http.HttpStatus.Code.CREATED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ApiErrorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiErrorTest.class);

    @Test
    public void testOk() {
        assertError(ApiError.ok("Complete"),
                "{\"code\":0,\"message\":\"Complete\"}", HttpStatus.Code.OK);
    }

    @Test
    public void testOkCreated() {
        assertError(ApiError.ok(CREATED, "Complete"),
                "{\"code\":0,\"message\":\"Complete\"}", HttpStatus.Code.CREATED);
    }

    @Test
    public void testInternalError() {
        assertError(ApiError.internalError(),
                "{\"code\":1,\"message\":\"Internal error\"}", HttpStatus.Code.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testFieldMissing() {
        assertError(ApiError.fieldMissing("code"),
                "{\"code\":2,\"message\":\"Field missing\",\"params\":{\"field\":\"code\"}}", HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void testInvalidJsonFormat() {
        assertError(ApiError.invalidJsonFormat(),
                "{\"code\":3,\"message\":\"Invalid json format\"}", HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void testInvalidFieldType() {
        assertError(ApiError.invalidFieldType("field1"),
                "{\"code\":4,\"message\":\"Invalid field type\",\"params\":{\"field\":\"field1\"}}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void testInvalidFieldFormat() {
        assertError(ApiError.invalidFieldFormat("field2"),
                "{\"code\":5,\"message\":\"Invalid field format\",\"params\":{\"field\":\"field2\"}}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void personNodeIdNotSuitable() {
        assertError(ApiError.personNodeIdNotSuitable(4L),
                "{\"code\":6,\"message\":\"Person node-id not suitable\",\"params\":{\"node-id\":\"4\"}}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void personNotFound() {
        assertError(ApiError.personNotFound(),
                "{\"code\":7,\"message\":\"Person not found\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void currencyNotFound() {
        assertError(ApiError.currencyNotFound(),
                "{\"code\":8,\"message\":\"Currency not found\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void accountNotFound() {
        assertError(ApiError.accountNotFound(),
                "{\"code\":9,\"message\":\"Account not found\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void accountNodeIdNotSuitable() {
        assertError(ApiError.accountNodeIdNotSuitable(4L),
                "{\"code\":10,\"message\":\"Account node-id not suitable\",\"params\":{\"node-id\":\"4\"}}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void crossNodeIdTransfersNotImplementedYet() {
        assertError(ApiError.crossNodeIdTransfersNotImplementedYet(),
                "{\"code\":11,\"message\":\"Cross node id transfers not implemented yet\"}",
                HttpStatus.Code.NOT_IMPLEMENTED);
    }

    @Test
    public void notPositiveAmount() {
        assertError(ApiError.notPositiveAmount(),
                "{\"code\":12,\"message\":\"Not positive amount\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void differentCurrenciesAccounts() {
        assertError(ApiError.differentCurrenciesAccounts(),
                "{\"code\":13,\"message\":\"Different currencies accounts\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void invalidAmountPrecision() {
        assertError(ApiError.invalidAmountPrecision(),
                "{\"code\":14,\"message\":\"Invalid amount precision\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void notEnoughMoney() {
        assertError(ApiError.notEnoughMoney(),
                "{\"code\":15,\"message\":\"Not enough money\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    @Test
    public void debitAndCreditAccountsAreEquals() {
        assertError(ApiError.debitAndCreditAccountsAreEquals(),
                "{\"code\":16,\"message\":\"Debit and credit accounts are equals\"}",
                HttpStatus.Code.BAD_REQUEST);
    }

    private void assertError(ApiHeader ok, String errorInJsonFormat, HttpStatus.Code expectedCode) {
        assertEquals(expectedCode, ok.getHttpCode());
        ok.setHttpCode(null);
        try {
            String actual = JsonUtil.json().render(ok);
            assertEquals(errorInJsonFormat, actual);
        } catch (Exception e) {
            LOGGER.error("Failed to render json");
            fail();
        }
    }

}