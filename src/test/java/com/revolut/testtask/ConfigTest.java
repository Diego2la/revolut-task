package com.revolut.testtask;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConfigTest {

    @Test
    public void testOk() {
        Config config = parser(new String[] {"-node-id", "4"});
        assertEquals(config.getNodeId().toString(), "4");
    }

    @Test
    public void testNoNodeId() {
        try {
            parser(new String[] {});
        } catch (ParameterException e) {
            assertEquals("The following option is required: [-node-id]", e.getMessage());
        }
    }

    @Test
    public void nodeInStr() {
        try {
            parser(new String[] {"-node-id", "fg"});
        } catch (ParameterException e) {
            assertEquals("\"-node-id\": couldn't convert \"fg\" to a long", e.getMessage());
        }
    }

    private Config parser(String[] argv) {
        Config config = new Config();
        JCommander.newBuilder()
                .addObject(config)
                .build()
                .parse(argv);
        return config;
    }
}
