package com.revolut.testtask;

import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpClientException;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.testtask.model.*;
import org.junit.ClassRule;
import org.junit.Test;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.servlet.SparkApplication;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.revolut.testtask.AccountService.ATOMIC_LONG_ACCOUNT_SEQ;
import static com.revolut.testtask.AccountService.MAP_ACCOUNTS;
import static com.revolut.testtask.ApiError.*;
import static com.revolut.testtask.Dictionary.CURRENCY_RUB;
import static com.revolut.testtask.Dictionary.CURRENCY_USD;
import static com.revolut.testtask.TransferService.ATOMIC_LONG_TRANSFER_SEQ;
import static com.revolut.testtask.TransferService.MAP_TRANSFERS;
import static org.eclipse.jetty.http.HttpStatus.Code.CREATED;
import static org.eclipse.jetty.http.HttpStatus.Code.OK;
import static org.junit.Assert.*;

public class MainTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainTest.class);

    public static TestAccountService accountService;
    public static class TestSparkApplication implements SparkApplication {
        @Override
        public void init() {
            Injector injector = Guice.createInjector(new TestAppModule());
            Main main = null;
            try {
                main = Main.main(injector, new String[] {"-node-id", "1"});
            } catch (IOException e) {
                LOGGER.error("Failed to start app", e);
                fail();
            }
            accountService = (TestAccountService) main.getAccountService();
        }
    }

    @ClassRule
    public static SparkServer<TestSparkApplication> testServer =
            new SparkServer<>(TestSparkApplication.class, 4567);

    private TestUtil util;
    private long testStartTime = System.currentTimeMillis();

    public MainTest() {
        util = new TestUtil(testServer);
    }

    @Test
    public void accountAddOk() {
        RedissonClient redisson = accountService.getRedis().getRedisson();
        RAtomicLong accountSeq = redisson.getAtomicLong(ATOMIC_LONG_ACCOUNT_SEQ);
        RMap<CrossNodeId, Account> accounts = redisson.getMap(MAP_ACCOUNTS);
        long seq = accountSeq.get();
        CrossNodeId crossNodeId = new CrossNodeId(1, seq + 1);
        assertNull(accounts.get(crossNodeId));

        util.assertResponse(new ApiResult(ok(CREATED, "Account successfully created"), crossNodeId),
                testServer.post("/account/add", "{\"person-id\": {\"node-id\": 1, \"id\": 6}, \"currency-code\": USD}", false));

        assertEquals(seq + 1, accountSeq.get());
        Account account = accounts.get(crossNodeId);
        assertNotNull(account);
        assertEquals(CURRENCY_USD.getCode(), account.getCurrencyCode());
        assertNull(account.getName());
        assertTrue(BigDecimal.ZERO.compareTo(account.getAmount()) == 0);
        assertEquals(crossNodeId, account.getId());
        assertEquals(new CrossNodeId(1, 6), account.getPersonId());
        assertTime(account.getTsCreated());
    }

    private void assertInvalid(ApiHeader header, String body) {
        util.assertResponse(new ApiResult(header),
                testServer.post("/account/add", body, false));
    }

    @Test
    public void validate() {
        assertInvalid(fieldMissing("person-id"), "{}");
        assertInvalid(invalidJsonFormat(),"{g:gg");
        assertInvalid(invalidFieldType("person-id"), "{\"person-id\": 5}");
        assertFieldFormatPersonId("{}");
        assertFieldFormatPersonId("{\"node-id\": \"ff\"}");
        assertFieldFormatPersonId("{\"node-id\": 1}");
        assertFieldFormatPersonId("{\"id\": \"ff\"}");
        assertFieldFormatPersonId("{\"id\": 2}");
        assertFieldFormatPersonId("{\"node-id\": {}, \"id\": 2}");

        String personId = "\"person-id\": {\"node-id\": 1, \"id\": 6}";
        assertInvalid(invalidFieldType("currency-code"), "{" + personId + ", \"currency-code\": {}}");
        assertInvalid(fieldMissing("currency-code"), "{" + personId + "}");
        assertInvalid(invalidFieldType("account-name"),
                "{\"person-id\": {\"node-id\": 1, \"id\": 6}, \"currency-code\": code7, \"account-name\": {}}");
        assertInvalid(personNodeIdNotSuitable(2L),
                "{\"person-id\": {\"node-id\": 2, \"id\": 6}, \"currency-code\": code7}");
        assertInvalid(personNotFound(),
                "{\"person-id\": {\"node-id\": 1, \"id\": 1006}, \"currency-code\": code7}");
        assertInvalid(currencyNotFound(),
                "{\"person-id\": {\"node-id\": 1, \"id\": 6}, \"currency-code\": code7}");
    }

    private void assertFieldFormatPersonId(String personId) {
        assertInvalid(invalidFieldFormat("person-id"), "{\"person-id\": " + personId + "}");
    }

    @Test
    public void internalError() {
        accountService.setThrowNpe(true);
        try {
            util.assertResponse(new ApiResult(ApiError.internalError()),
                    testServer.post("/account/add", "", false));
        } finally {
            accountService.setThrowNpe(false);
        }
    }

    private void assertInvalidGetBalance(ApiHeader header, String params) {
        util.assertResponse(new ApiResult(header),
                testServer.get("/account/get-balance" + params, false));
    }

    @Test
    public void accountGetBalance() {
        assertInvalidGetBalance(ApiError.fieldMissing("account-id.id"), "");
        assertInvalidGetBalance(ApiError.invalidFieldType("account-id.id"), "?account-id.id=r");
        assertInvalidGetBalance(ApiError.fieldMissing("account-id.node-id"),
                "?account-id.id=1");
        assertInvalidGetBalance(ApiError.invalidFieldType("account-id.node-id"),
                "?account-id.id=1&account-id.node-id=r");
        assertInvalidGetBalance(ApiError.accountNodeIdNotSuitable(9L),
                "?account-id.id=1&account-id.node-id=9");
        assertInvalidGetBalance(ApiError.accountNotFound(),
                "?account-id.id=400&account-id.node-id=1");

        RedissonClient redisson = accountService.getRedis().getRedisson();
        RAtomicLong accountSeq = redisson.getAtomicLong(ATOMIC_LONG_ACCOUNT_SEQ);
        long seq = accountSeq.get();
        CrossNodeId crossNodeId = new CrossNodeId(1, seq + 1);
        util.assertResponse(new ApiResult(ok(CREATED, "Account successfully created"), crossNodeId),
                testServer.post("/account/add", "{\"person-id\": {\"node-id\": 1, \"id\": 16}, \"currency-code\": USD}", false));

        String url = "/account/get-balance?account-id.id=" + crossNodeId.getId()
                + "&account-id.node-id=" + crossNodeId.getNodeId();
        LOGGER.info("url = {}", url);
        util.assertResponse(new ApiResult(ok(OK, "Retrieving amount complete"), BigDecimal.ZERO),
                testServer.get(url, false));
    }

    private BigDecimal getBalance(CrossNodeId crossNodeId) {
        String url = "/account/get-balance?account-id.id=" + crossNodeId.getId()
                + "&account-id.node-id=" + crossNodeId.getNodeId();
        GetMethod get = testServer.get(url, false);
        HttpResponse httpResponse = null;
        try {
            httpResponse = testServer.execute(get);
        } catch (HttpClientException e) {
            LOGGER.error("Failed to render json", e);
        }
        Map<String, Double> actual = new Gson().fromJson(new String(httpResponse.body()), HashMap.class);
        Double data = actual.get("data");
        return new BigDecimal(data.toString());
    }

    private void assertTransferEmission(ApiHeader header, String body) {
        util.assertResponse(new ApiResult(header),
                testServer.post("/transfer/emission", body, false));
    }

    @Test
    public void transferEmission() {
        assertTransferEmission(ApiError.invalidJsonFormat(), "{");
        assertTransferEmission(ApiError.invalidFieldType("credit-account-id"),
                "{\"credit-account-id\":6}");
        assertTransferEmission(ApiError.invalidFieldFormat("credit-account-id"),
                "{\"credit-account-id\":{\"id\":foo, \"node-id\":6}}");
        assertTransferEmission(ApiError.invalidFieldFormat("credit-account-id"),
                "{\"credit-account-id\":{\"id\":5, \"node-id\":{}}}");
        assertTransferEmission(ApiError.invalidFieldType("amount"),
                "{\"credit-account-id\":{\"id\":1, \"node-id\":1}, \"amount\": f}");
        assertTransferEmission(ApiError.notPositiveAmount(),
                "{\"credit-account-id\":{\"id\":1, \"node-id\":1}, \"amount\": -6.8}");
        assertTransferEmission(ApiError.accountNodeIdNotSuitable(2L),
                "{\"credit-account-id\":{\"id\":1, \"node-id\":2}, \"amount\": 8}");
        assertTransferEmission(ApiError.accountNotFound(),
                "{\"credit-account-id\":{\"id\":1001, \"node-id\":1}, \"amount\": 8}");

        CrossNodeId accountId = createAccount(new CrossNodeId(1, 4), CURRENCY_USD.getCode());

        assertTransferEmission(ApiError.invalidAmountPrecision(),
                "{\"credit-account-id\":{\"id\":" + accountId.getId()
                        + ", \"node-id\":" + accountId.getNodeId() + "}, \"amount\": 8.222}");

        assertTrue(BigDecimal.ZERO.compareTo(getBalance(accountId)) == 0);

        RedissonClient redisson = accountService.getRedis().getRedisson();
        RAtomicLong nextTransactionId = redisson.getAtomicLong(ATOMIC_LONG_TRANSFER_SEQ);
        long nextId = nextTransactionId.get() + 1;
        CrossNodeId transferId = new CrossNodeId(1L, nextId);
        RMap<CrossNodeId, Transfer> transfers = redisson.getMap(MAP_TRANSFERS);
        assertNull(transfers.get(transferId));

        assertTransferEmission(ApiError.ok(CREATED, "Emission performed successfully"),
                "{\"credit-account-id\":{\"id\":" + accountId.getId()
                        + ", \"node-id\":" + accountId.getNodeId() + "}, \"amount\": 4.50}");

        Transfer transfer = transfers.get(transferId);
        assertNotNull(transfer);
        assertEquals(transferId, transfer.getId());
        assertEquals(TransferType.EMISSION, transfer.getType());
        assertEquals(accountId, transfer.getAccountId());
        assertNull(transfer.getCpAccountId());
        assertTrue(new BigDecimal("4.50").compareTo(transfer.getAmount()) == 0);
        assertTime(transfer.getTsCreated());
        assertEquals(CURRENCY_USD.getCode(), transfer.getCurrencyCode());

        assertTrue(new BigDecimal("4.50").compareTo(getBalance(accountId)) == 0);
    }

    private void transferEmission(CrossNodeId accountId, BigDecimal amount) {
        assertTransferEmission(ApiError.ok(CREATED, "Emission performed successfully"),
                "{\"credit-account-id\":{\"id\":" + accountId.getId()
                        + ", \"node-id\":" + accountId.getNodeId() + "}, \"amount\": " + amount + "}");
    }

    private void assertTime(long ts) {
        assertTrue(testStartTime <= ts);
        assertTrue(ts <= System.currentTimeMillis());
    }

    private CrossNodeId createAccount(CrossNodeId personId, String currencyCode) {
        RedissonClient redisson = accountService.getRedis().getRedisson();
        RAtomicLong accountSeq = redisson.getAtomicLong(ATOMIC_LONG_ACCOUNT_SEQ);
        long seq = accountSeq.get();
        CrossNodeId crossNodeId = new CrossNodeId(1, seq + 1);
        util.assertResponse(new ApiResult(ok(CREATED, "Account successfully created"), crossNodeId),
                testServer.post("/account/add", "{\"person-id\": {\"node-id\": " + personId.getNodeId()
                        + ", \"id\": " + personId.getId() + "}, \"currency-code\": " + currencyCode + "}", false));
        return crossNodeId;
    }

    private void assertTransferAdd(ApiHeader header, String body) {
        util.assertResponse(new ApiResult(header),
                testServer.post("/transfer/add", body, false));
    }

    @Test
    public void transferAdd() {
        assertTransferAdd(invalidJsonFormat(), "{");
        assertTransferAdd(ApiError.fieldMissing("debit-account-id"),
                "{}");
        assertTransferAdd(ApiError.invalidFieldType("debit-account-id"),
                "{\"debit-account-id\":6}");
        assertTransferAdd(ApiError.invalidFieldFormat("debit-account-id"),
                "{\"debit-account-id\":{\"id\":foo, \"node-id\":6}}");
        assertTransferAdd(ApiError.invalidFieldFormat("debit-account-id"),
                "{\"debit-account-id\":{\"id\":5, \"node-id\":{}}}");
        String debit = "\"debit-account-id\":{\"id\":6, \"node-id\":1}";
        assertTransferAdd(ApiError.fieldMissing("credit-account-id"),
                "{" + debit + "}");
        assertTransferAdd(ApiError.invalidFieldType("credit-account-id"),
                "{" + debit + ", \"credit-account-id\":6}");
        assertTransferAdd(ApiError.invalidFieldFormat("credit-account-id"),
                "{" + debit + ", \"credit-account-id\":{\"id\":foo, \"node-id\":6}}");
        assertTransferAdd(ApiError.invalidFieldFormat("credit-account-id"),
                "{" + debit + ", \"credit-account-id\":{\"id\":5, \"node-id\":{}}}");


        assertTransferAdd(ApiError.invalidFieldType("amount"),
                transferAddJson(new CrossNodeId(1, 2), new CrossNodeId(1, 3), "g"));
        assertTransferAdd(ApiError.notPositiveAmount(),
                transferAddJson(new CrossNodeId(1, 2), new CrossNodeId(1, 3), "-8"));
        assertTransferAdd(ApiError.accountNodeIdNotSuitable(2L),
                transferAddJson(new CrossNodeId(2, 2), new CrossNodeId(1, 3), "1"));
        assertTransferAdd(ApiError.crossNodeIdTransfersNotImplementedYet(),
                transferAddJson(new CrossNodeId(1, 2), new CrossNodeId(2, 3), "1"));

        assertTransferAdd(ApiError.accountNotFound(),
                transferAddJson(new CrossNodeId(1, 5), new CrossNodeId(1, 6), "1"));
        CrossNodeId personId = new CrossNodeId(1, 18);
        CrossNodeId accountUsdA = createAccount(personId, CURRENCY_USD.getCode());
        CrossNodeId accountUsdB = createAccount(personId, CURRENCY_USD.getCode());
        CrossNodeId accountRubA = createAccount(personId, CURRENCY_RUB.getCode());
        assertTransferAdd(ApiError.accountNotFound(),
                transferAddJson(accountUsdA, new CrossNodeId(1, 6), "1"));
        assertTransferAdd(ApiError.debitAndCreditAccountsAreEquals(),
                transferAddJson(accountUsdA, accountUsdA, "1"));
        assertTransferAdd(ApiError.differentCurrenciesAccounts(),
                transferAddJson(accountUsdA, accountRubA, "1"));
        assertTransferAdd(ApiError.invalidAmountPrecision(),
                transferAddJson(accountUsdA, accountUsdB, "1.222"));
        assertTransferAdd(ApiError.notEnoughMoney(),
                transferAddJson(accountUsdA, accountUsdB, "1"));

        transferEmission(accountUsdA, new BigDecimal("1"));

        RedissonClient redisson = accountService.getRedis().getRedisson();
        RAtomicLong nextTransactionId = redisson.getAtomicLong(ATOMIC_LONG_TRANSFER_SEQ);
        CrossNodeId transferIdDebit = new CrossNodeId(1L, nextTransactionId.get() + 1);
        CrossNodeId transferIdCredit = new CrossNodeId(1L, nextTransactionId.get() + 2);
        RMap<CrossNodeId, Transfer> transfers = redisson.getMap(MAP_TRANSFERS);
        assertNull(transfers.get(transferIdDebit));

        assertTransferAdd(ApiError.ok(CREATED, "Transfer performed successfully"),
                transferAddJson(accountUsdA, accountUsdB, "1"));
        BigDecimal balance = getBalance(accountUsdA);
        assertTrue(balance.compareTo(BigDecimal.ZERO) == 0);
        assertTrue(getBalance(accountUsdB).compareTo(BigDecimal.ONE) == 0);

        Transfer transferDebit = transfers.get(transferIdDebit);
        assertNotNull(transferDebit);
        assertEquals(transferIdDebit, transferDebit.getId());
        assertEquals(TransferType.WITHDRAW, transferDebit.getType());
        assertEquals(accountUsdA, transferDebit.getAccountId());
        assertEquals(accountUsdB, transferDebit.getCpAccountId());
        assertTrue(new BigDecimal("1").compareTo(transferDebit.getAmount()) == 0);
        assertTime(transferDebit.getTsCreated());
        assertEquals(CURRENCY_USD.getCode(), transferDebit.getCurrencyCode());

        Transfer transferCredit = transfers.get(transferIdCredit);
        assertNotNull(transferCredit);
        assertEquals(transferIdCredit, transferCredit.getId());
        assertEquals(TransferType.INPUT, transferCredit.getType());
        assertEquals(accountUsdB, transferCredit.getAccountId());
        assertEquals(accountUsdA, transferCredit.getCpAccountId());
        assertTrue(new BigDecimal("1").compareTo(transferCredit.getAmount()) == 0);
        assertTime(transferCredit.getTsCreated());
        assertEquals(CURRENCY_USD.getCode(), transferCredit.getCurrencyCode());
    }

    private void transferAdd(CrossNodeId debit, CrossNodeId credit, String amount) {
        String body = transferAddJson(debit, credit, amount);
        PostMethod post = testServer.post("/transfer/add", body, false);
        HttpResponse execute = null;
        try {
            execute = testServer.execute(post);
        } catch (HttpClientException e) {
            LOGGER.error("Failed to execute", e);
            fail();
        }
        LOGGER.info(new String(execute.body()));
    }

    @Test
    public void complexTest() {
        CrossNodeId personAId = new CrossNodeId(1, 19);
        CrossNodeId accountUsdA = createAccount(personAId, CURRENCY_USD.getCode());
        CrossNodeId accountUsdB = createAccount(personAId, CURRENCY_USD.getCode());
        transferEmission(accountUsdA, new BigDecimal("10.50"));
        transferAdd(accountUsdA, accountUsdB, "3.30");
        assertTrue(new BigDecimal("3.30").compareTo(getBalance(accountUsdB)) == 0);
        transferEmission(accountUsdA, new BigDecimal("1"));
        transferAdd(accountUsdB, accountUsdA, "2");
        BigDecimal balanceUpdated = getBalance(accountUsdB);
        LOGGER.info("balanceUpdated={}", balanceUpdated);
        assertTrue(new BigDecimal("1.30").compareTo(balanceUpdated) == 0);
    }

    private String transferAddJson(CrossNodeId debit, CrossNodeId credit, String amount) {
        return "{\"debit-account-id\": {\"id\":" + debit.getId() + ", \"node-id\": " + debit.getNodeId() + "}," +
                "\"credit-account-id\":{\"id\":" + credit.getId() + ", \"node-id\":" + credit.getNodeId()
                + "}, \"amount\": " + amount + "}";
    }
}
