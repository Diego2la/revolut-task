package com.revolut.testtask;

import com.google.inject.Inject;
import com.revolut.testtask.model.ApiResult;

public class TestAccountService extends AccountService {

    private boolean isThrowNpe;

    @Inject
    public TestAccountService(Config config, Dictionary dictionary, EmbeddedRedisHolder redisHolder) {
        super(config, dictionary, redisHolder);
        this.isThrowNpe = false;
    }

    @Override
    public ApiResult create(String body) throws HandlerException {
        if (!isThrowNpe) {
            return super.create(body);
        } else {
            throw new NullPointerException();
        }
    }

    public void setThrowNpe(boolean throwNpe) {
        isThrowNpe = throwNpe;
    }
}
