package com.revolut.testtask;

import com.google.inject.Binder;

public class TestAppModule extends AppModule {

    @Override
    public void configure(Binder binder) {
        binder.bind(IAccountService.class).to(TestAccountService.class);
        binder.bind(ITransferService.class).to(TransferService.class);
        binder.bind(Config.class).asEagerSingleton();
        binder.bind(Dictionary.class).asEagerSingleton();
        binder.bind(EmbeddedRedisHolder.class).asEagerSingleton();
    }
}
