package com.revolut.testtask;

import com.despegar.http.client.HttpClientException;
import com.despegar.http.client.HttpMethod;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.Gson;
import com.revolut.testtask.model.ApiHeader;
import com.revolut.testtask.model.ApiResult;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.revolut.testtask.util.JsonUtil.json;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class TestUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtil.class);

    private SparkServer testServer;

    public TestUtil(SparkServer testServer) {
        this.testServer = testServer;
    }

    public <D> D assertResponse(ApiResult<D> result, HttpMethod httpMethod) {
        HttpResponse httpResponse = null;
        try {
            httpResponse = testServer.execute(httpMethod);
        } catch (HttpClientException e) {
            LOGGER.error("Failed to post", e);
            fail();
        }
        ApiHeader header = result.getHeader();
        HttpStatus.Code headerHttpCode = header.getHttpCode();
        assertEquals(Arrays.asList("application/json"), httpResponse.headers().get("Content-Type"));
        header.setHttpCode(null);
        Map<String, String> expected = null;
        try {
            expected = parseJson(json().render(result));
        } catch (Exception e) {
            LOGGER.error("Failed to render json", e);
        }
        Map<String, String> actual = parseJson(new String(httpResponse.body()));
        assertEquals(expected, actual);
        assertEquals(headerHttpCode.getCode(), httpResponse.code());
        assertNotNull(testServer.getApplication());
        return result.getData();
    }

    public static Map<String,String> parseJson(String body) {
        return new Gson().fromJson(body, HashMap.class);
    }
}
